from __future__ import annotations

import random
import textwrap
from html.parser import HTMLParser
from pathlib import Path
from subprocess import PIPE, Popen
from typing import List, Optional

import mkdocs
from mkdocs.plugins import BasePlugin
from mkdocs.structure.pages import Page


class Term:
    """
    Data class that represents a term and its associated term.

    Attributes:
    -----------
    term : str
        The term associated with the term.

    url : str
        The URL of the page the term comes from.

    content : str
        The content of the term.
    """

    def __init__(
        self,
        term: str | None = None,
        url: str | None = None,
        content: str | None = None,
    ):
        self.term = term
        self.content = content
        self.url = url

    def to_dict(self):
        """
        Returns the term as a dictionary.
        """
        return {"term": self.term, "content": self.content, "url": self.url}


class TermExtractor(HTMLParser):
    """
    HTML parser that extracts terms and their associated terms from an HTML document.

    Attributes:
    -----------
    url : str
        The URL of the HTML document being parsed.

    terms : List[Term]
        A list that stores Term objects representing the extracted terms.

    current_term : Optional[Term]
        The Term object currently being processed.

    current_content : Optional[str]
        A string that accumulates the content of the term being processed.
    """

    def __init__(self, url: str):
        """
        Initializes a new TermExtractor instance.

        Parameters:
        -----------
        url : str
            The URL of the HTML document being parsed.
        """
        super().__init__()
        self.url = url
        self.terms: list[Term] = []
        self.current_term: Term | None = None
        self.current_content: str | None = None

    def handle_starttag(self, tag: str, attrs: list[tuple[str, str]]):
        """
        Extracts the term of a term from its data-term attribute.
        """
        if tag == "b":
            for attr in attrs:
                if attr[0] == "content":
                    self.current_term = Term(term=attr[1])

    def handle_data(self, data: str):
        """
        Extracts the content of a term and stores it in the current_content attribute.
        """
        if self.current_term is not None:
            if self.current_content is None:
                self.current_content = data
            else:
                self.current_content += data

    def handle_endtag(self, tag: str):
        """
        Adds the current term object to the terms list when the end tag of the
        term is encountered.
        """
        if tag == "b" and self.current_term is not None:
            self.current_term.content = self.current_content or ""
            self.terms.append(self.current_term)
            self.current_term = None
            self.current_content = None


class MapPlugin(BasePlugin):
    """
    MkDocs plugin to generate a graph visualization with associated terms.

    This plugin extracts terms from HTML content and generates a graph visualization
    with associated terms using Graphviz.

    Usage:
    ------
    1. Add this plugin to your MkDocs configuration:
       plugins:
         - map

    2. In your Markdown content, use the <!-- MAP --> comment to indicate where
       the generated graph should be placed.

    3. Customize the graph appearance and behavior by modifying the code within
       the `_wrap` method and other relevant parts of this plugin.
    """

    def __init__(self):
        self.code = [
            """digraph map {
            graph[
                overlap=false, 
                smoothing=spring,
                splines=true,
            ]
            node [
                fontname="Fluxisch Else",
                penwidth=2,
                shape=box
            ]
            edge []"""
        ]

    def _wrap(self, value, width=30):
        """
        Wrap text to fit within a specified width.

        Parameters:
        -----------
        value : str
            The text to wrap.

        width : int, optional
            The maximum width for the wrapped text.

        Returns:
        --------
        str
            The wrapped text.
        """
        return "\\n".join(
            textwrap.wrap(value, width=width, break_long_words=False)
        )

    def on_nav(
        self,
        nav: mkdocs.structure.nav.Navigation,
        config: mkdocs.config.base.Config,
        files: list[mkdocs.structure.files.File],
    ) -> mkdocs.structure.nav.Navigation:
        """
        Hook called before the navigation structure is built.

        Parameters:
        -----------
        nav : mkdocs.structure.nav.Navigation
            The navigation structure.

        config : mkdocs.config.base.Config
            The MkDocs configuration.

        files : List[mkdocs.structure.files.File]
            The list of files in the documentation.

        Returns:
        --------
        mkdocs.structure.nav.Navigation
            The modified navigation structure.
        """
        self.homepage = nav.homepage
        return nav

    def on_post_page(
        self,
        output: str,
        page: Page,
        config: mkdocs.config.base.Config,
        **kwargs,
    ) -> str:
        """
        Hook called after a page is rendered to HTML.

        Parameters:
        -----------
        output : str
            The rendered HTML content of the page.

        page : Page
            The Page object representing the current page.

        config : mkdocs.config.base.Config
            The MkDocs configuration.

        Returns:
        --------
        str
            The modified HTML content.
        """
        colors = ["red", "blue", "green", "black"]

        if page.is_page:
            depth = len(list(filter(lambda x: x.is_page, page.ancestors)))
            color = colors[depth]
            directions = ["c"]

            self.code.append(
                '"{}" [href="{}", color="{}" fontcolor="{}"]'.format(
                    self._wrap(page.title), page.abs_url, color, color
                )
            )

            parent = page.parent

            while parent:
                if parent.is_page:
                    break
                parent = parent.parent

            if parent:
                self.code.append(
                    '"{}":{} -> "{}":{}'.format(
                        self._wrap(parent.title),
                        random.choice(directions),
                        self._wrap(page.title),
                        random.choice(directions),
                    )
                )
            elif not page.is_homepage:
                self.code.append(
                    '"{}":{} -> "{}":{}'.format(
                        self._wrap(self.homepage.title),
                        random.choice(directions),
                        self._wrap(page.title),
                        random.choice(directions),
                    )
                )

        parser = TermExtractor(url=f"/{page.url}")
        parser.feed(output)

        for term in parser.terms:
            self.code.append(
                '"{}" [shape="plain"]'.format(
                    term.term,
                )
            )
            self.code.append(
                '"{}":{} -> "{}":{}'.format(
                    self._wrap(page.title),
                    random.choice(directions),
                    term.term,
                    random.choice(directions),
                )
            )

        return output

    def on_post_build(self, config: mkdocs.config.base.Config) -> None:
        """
        Hook called after the documentation site is built.

        Parameters:
        -----------
        config : mkdocs.config.base.Config
            The MkDocs configuration.
        """
        self.code.append("}")

        p = Popen(["neato", "-Tsvg"], stdout=PIPE, stdin=PIPE, stderr=PIPE)
        svg = p.communicate(input="\n".join(self.code).encode())[0]

        root = Path(config["site_dir"])

        for path in root.rglob("*.html"):
            if not path.name.startswith(".") and path.is_file():
                with open(path, mode="r+") as file:
                    content = file.read()
                    content = content.replace(
                        "<!-- MAP -->", svg.decode("utf-8")
                    )
                    file.seek(0)
                    file.write(content)


# For MkDocs to recognize the plugin
plugin = MapPlugin()
