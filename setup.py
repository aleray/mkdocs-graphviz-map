from setuptools import setup, find_packages

setup(
    name='mkdocs_map',
    version='0.0.1',
    description='A plugin to generate indexes',
    url='https://tangible-cloud.be/',
    author='Alex',
    author_email='alexandre@stdin.fr',
    packages=find_packages(),
    install_requires=[],
    zip_safe=False,
    entry_points={
        'mkdocs.plugins': [
            'map = mkdocs_map.plugin:MapPlugin',
        ]
    }
)
