# MkDocs Map Plugin

The MkDocs Map Plugin generates an SVG graph visualization from tagged terms for your MkDocs documentation. This plugin extracts terms from HTML content and creates a graph using Graphviz, visualizing relationships between terms on your documentation pages.

## Installation

Install Graphviz command-line program. Procedure depend on your operating system.

Install the plugin using pip:

```bash
pip install mkdocs-map-plugin
```

Add the following lines to your `mkdocs.yml`:

```yaml
plugins:
  - map
```

In your Markdown content, use the `<!-- MAP -->` comment to indicate where the generated graph should be placed.

Customize the graph appearance and behavior by modifying the code within the `_wrap` method and other relevant parts of this plugin.

## Usage

### Placing Graph in Markdown

To indicate where the generated graph should be placed in your Markdown content, add the `<!-- MAP -->` comment:

```markdown
<!-- MAP -->
```

The plugin will replace this comment with the generated graph during the build process.

## Example

```markdown
# My Documentation

Welcome to my documentation!

<!-- MAP -->
```

## License

License yet to come.
